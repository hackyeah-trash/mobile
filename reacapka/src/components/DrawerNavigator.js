import {SafeAreaView, ScrollView, StyleSheet, View, Text} from "react-native";
import {Icon} from "react-native-elements";
import {createDrawerNavigator, DrawerNavigatorItems} from "react-navigation-drawer";
import React from "react";

import HomeScreen from "../screens/HomeScreen/HomeScreen";
import HomeScreenWrapper from '../screens/HomeScreen/ScreenWrapper';
import LoginScreen from '../screens/login/LoginScreen';
import RegistrationScreen from '../screens/registration/RegistrationScreen';
import WelcomeScreen from '../screens/welcome/WelcomeScreen';
import MyEcopointsScreen from "../screens/myEcopoints/MyEcopointsScreen";

import {createAppContainer} from "react-navigation";
import AchievementsScreen from "../screens/achievements/AchievementsScreen";
import CollectionPointOwnerHomeScreen from "../screens/HomeScreen/CollectionPointOwnerHomeScreen";

const CustomDrawerComponent = (props) => (
    <SafeAreaView style={{flex: 1}}>
        <View style={styles.menuImage}>
            <Icon name='account-circle'
                size={80}
                color='#2196F3'/>
            <Text style={styles.pointsText}>Twoje punkty: 1500</Text>
        </View>
        <ScrollView>
            <DrawerNavigatorItems {...props} />
        </ScrollView>
    </SafeAreaView>
);

const DrawerNavigator = createDrawerNavigator({
    Welcome: {
        screen: WelcomeScreen,
    },
    Home: {
        screen: HomeScreenWrapper,
    },
    Login: {
        screen: LoginScreen,
    },
    Register: {
        screen: RegistrationScreen,
    },
    MyEcopoints: {
        screen: MyEcopointsScreen,
    },
    Achievements: {
        screen: AchievementsScreen
    },
    CollectionOwner: {
        screen: CollectionPointOwnerHomeScreen
    }
}, {
    contentComponent: CustomDrawerComponent,
    contentOptions: {
        labelStyle: {color: '#8a8a8a'},
    },
});

const styles = StyleSheet.create({
    menuImage: {
        height: 100,
        marginTop: 10,
        backgroundColor: 'white',
    },
    pointsText: {
        color: '#2196F3',
        // fontFamily: 'Roboto',
        textAlign: 'center',
        fontSize: 18,
        height: 50
    }
});

export default createAppContainer(DrawerNavigator);

