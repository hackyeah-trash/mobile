import React from 'react';
import {Icon} from 'react-native-elements';
import {
  TouchableOpacity,
  StyleSheet,
  Animated
} from 'react-native';

const AnimatedTouchableOpacity = Animated.createAnimatedComponent(TouchableOpacity);

const Button = ({action, buttonStyle, size=null, iconName=null, textStyle=null, text="", iconColor="#ffffff", hitSlop=null, type}) => {
  return (
      <AnimatedTouchableOpacity
        style={buttonStyle}
        onPress={action}
        hitSlop={hitSlop}>
        {iconName !== null ?
          <Icon name={iconName} type={type ? type : "MaterialIcons"} size={size} color={iconColor} />
          :
          <Animated.Text style={[styles.text, textStyle]}>{text}</Animated.Text>
        }
        
      </AnimatedTouchableOpacity>
  )
};

const styles = StyleSheet.create({
  text: {
    textAlign: 'center'
  }
});

export default Button;
