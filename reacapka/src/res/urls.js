const baseUrl = 'http://10.250.164.116:8080/api';

export const authUrl = `${baseUrl}/user/login`;
export const registerUrl = `${baseUrl}/user/register`;
export const userUrl = username => `${baseUrl}/user/${username}`;
export const collectionPointsUrl = `${baseUrl}/collectionpoints`;
export const collectionPointUrl = collectionPointId => `${baseUrl}/collectionpoints/${collectionPointId}`;
export const storesUrl = `${baseUrl}/stores`;
export const storeUrl = storeId => `${baseUrl}/stores/${storeId}`;
export const materialsUrl = `${baseUrl}/materials`;
export const materialUrl = materialId => `${baseUrl}/materials/${materialId}`;
export const ecopointsUrl = username => `${baseUrl}/user/${username}/ecopoints`;
