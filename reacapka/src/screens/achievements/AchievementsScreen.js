import {StyleSheet, Image, View, Text, Dimensions} from "react-native";
import React from "react";
import {Icon} from 'react-native-elements';
import {THEME_GREEN} from "../../res/colors";

export default class AchievementsScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Achievements',
        drawerIcon: () => (
            <Icon
                type="material-community"
                name='medal'
                size={25}
                color="#4CAF50" />
        ),
    };

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <View style={styles.body}>
                {/*<View style={styles.materialImage}>*/}
                {/*    <Image source={require('../../assets/paper.png')} style={{width: 32, height: 32}} resizeMode={"contain"} />*/}
                {/*</View>*/}
            </View>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
    },
    materialImage: {
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.15,
        height: Dimensions.get('window').width * 0.15,
        backgroundColor: '#c787f5',
        borderWidth: 5,
        borderColor: '#c787f5',
        marginLeft: 15,
        marginTop: 10
    },
});