import {Overlay} from "react-native-elements";
import {StyleSheet, Text, View} from "react-native";
import QRCode from "react-native-qrcode-svg";
import React from "react";
import Button from "../../components/Button";
import {THEME_GREEN} from "../../res/colors";

const QRDialog = (props) => {
    return (
            <Overlay isVisible={props.isVisible} height={280}>
                <View style={styles.container}>
                    <Text style={styles.title}>Twój kod QR:</Text>
                    <View style={{marginTop: 10}}>
                        <QRCode value="http://awesome.link.qr" size={140} />
                    </View>
                    <Button
                        text="OK"
                        buttonStyle={styles.button}
                        textStyle={styles.buttonText}
                        action={() => props.onClose()} />
                </View>
            </Overlay>
    )
};


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center'
    },
    title: {
        // fontFamily: 'Roboto',
        fontSize: 24,
        fontWeight: 'bold',
        color: '#4CAF50'
    },
    button: {
        alignSelf: 'center',
        backgroundColor: THEME_GREEN,
        marginTop: 20,
        borderRadius: 10,
        padding: 10,
        width: '40%',
    },
    buttonText: {
        fontSize: 20,
        color: '#fff'
    }
});

export default QRDialog;