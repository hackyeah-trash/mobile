import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Text,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon} from 'react-native-elements';
import CollectionPointOwnerHomeScreen from './CollectionPointOwnerHomeScreen';
import HomeScreen from './HomeScreen';
import {getIsCollectionPointOwner} from '../../redux/selectors/authSelector';
import Button from '../../components/Button';
import { THEME_GREEN } from '../../res/colors';

class ScreenWrapper extends Component {
  static navigationOptions = {
    drawerLabel: 'Home',
    drawerIcon: () => (
      <Icon
        type="font-awesome"
        name='home'
        size={25}
        color="#4CAF50"
      />
    ),
  };
  render() {
    // const {isCollectionPointOwner} = this.props;
    const isCollectionPointOwner = true;
    return (
      <View style={styles.container}>
        <View style={styles.header}> 
          <Text style={styles.headerTitle}>Cześć, Jan Kowalski!</Text>
          <Button 
            type="material-community"
            iconName="account-circle-outline"
            iconColor={THEME_GREEN}
            size={40}
            buttonStyle={styles.headerBtn}
            action={() => this.props.navigation.openDrawer()}
          />
          <Button
            type="material-community"
            iconName="qrcode"
            iconColor={THEME_GREEN}
            size={40}
            buttonStyle={styles.headerBtn}
          />
        </View>
          <HomeScreen/>
      </View>
    );
  }
}

const styles=StyleSheet.create({
  container: {
    flex: 1,
  },
  header: {
    height: 60,
    padding: 5,
    alignItems: 'center',
    flexDirection: "row",
    justifyContent: "flex-end",
    backgroundColor: "#fff",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 7,
    },
    shadowOpacity: 0.21,
    shadowRadius: 9.11,
    elevation: 14,
  },
  headerItem: {
    marginRight: 15
  },
  headerTitle: {
      flex: 1,
      textAlign: "left",
      marginLeft: 15,
      marginTop: 5,
      fontSize: 18,
      // fontFamily: 'Roboto',
  },
  headerBtn: {
    margin: 5,
  }
})

mapStateToProps = (state) => ({
  isCollectionPointOwner: getIsCollectionPointOwner(state)
});

export default connect(
  mapStateToProps,
  null
)(ScreenWrapper);