import {Alert, StyleSheet, Text, TouchableOpacity, View, Image} from "react-native";
import React from "react";
import {Icon} from 'react-native-elements';
import MapView, {AnimatedRegion, PROVIDER_GOOGLE, Polyline, Marker} from 'react-native-maps';
import Permissions from 'react-native-permissions';
import Geolocation from 'react-native-geolocation-service';
import Button from "../../components/Button";
import QRDialog from "./QRDialog";
import {THEME_GREEN} from '../../res/colors';

import {makeAction} from "../../redux/actions/makeAction";
import { FETCH_COLLECTION_POINTS} from "../../redux/actions/actionTypes";
import {FETCH_STORES} from "../../redux/actions/actionTypes";
import {connect} from "react-redux";
import {getUser} from "../../redux/selectors/authSelector";
import {getCollectionPoints} from "../../redux/selectors/collectionPointsSelector";
import {getStores} from "../../redux/selectors/storesSelector";

const LATITUDE_DELTA = 0.009;
const LONGITUDE_DELTA = 0.009;

class HomeScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'Home',
        drawerIcon: () => (
            <Icon
                type="font-awesome"
                name='home'
                size={25}
                color="#4CAF50" />
        ),
    };

    constructor(props) {
        super(props);
        this.state = {
            latitude: 0,
            longitude: 0,
            hasPermission: false,
            qrDialogVisibility: true,
            selectedFilter: "CP"
        };

        this.myAccountOnClick = this.myAccountOnClick.bind(this);
    }

    componentDidMount() {
        
        Permissions.check('location', {type: 'whenInUse'}).then(response => {
            if (response === "authorized") {

                const data = {
                    lat: this.state.latitude,
                    lng: this.state.longitude,
                    radius: 500
                };

                this.props.fetchCollectionPoints({
                    data: data
                });
                this.setState({hasPermission: true});
                this.getLocation();
            } else {
                this._requestPermission();
                this.getLocation();
            }
        });
    }

    getMapRegion = () => ({
        latitude: this.state.latitude,
        longitude: this.state.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
    });

    _requestPermission = () => {
        Permissions.request('location', {type: 'whenInUse'}).then(response => {
            this.setState({hasPermission: response === "authorized"});
        })
    };

    getLocation = () => {
        Geolocation.watchPosition(
            (position) => {
                this.setState({
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            },
            (error) => {
                console.error(error.code, error.message);
            }, {
                enableHighAccuracy: true, timeout: 15000, maximumAge: 10000
            }
        );
    };

    getMarkerDescription(marker) {
        if (!marker.materials) return "";

        let string = "";
        marker.materials.forEach((material) => {
            string += material.name + " ";
        });
        return string;
    }

    render() {
        let markers = this.state.selectedFilter === "CP" ? this.props.collectionPoints : this.props.stores;
        if (!markers) markers = [];
        return (
            <View style={styles.body}>
                <View style={styles.mapContainer}>
                    <MapView
                        style={styles.map}
                        provider={PROVIDER_GOOGLE}
                        showsUserLocation={true}
                        showsMyLocationButton={true}
                        region={this.getMapRegion()}>
                        {markers.map(marker => (
                            <MapView.Marker
                                key={marker.id}
                                coordinate={{latitude: marker.location.latitude, longitude: marker.location.longitude}}
                                title={marker.name}
                                description={this.getMarkerDescription(marker)}>
                                {this.state.selectedFilter === "CP" ? <Icon size={24} name={"recycle"} type={'font-awesome'}/> : <Icon size={24} name={"shoppingcart"} type={'antdesign'}/>}
                            </MapView.Marker>
                        ))}
                    </MapView>
                </View>
                <View style={styles.expandMenu}>
                    <Button textStyle={[styles.menuButtonText, {...this.state.selectedFilter === "CP" && {textDecorationLine:'underline'}} ]} buttonStyle={{width: '50%'}} text={"Collection points"}
                        action={() => this.onCollPointButtonClicked()}/>
                    <View style={{width: 1, backgroundColor: 'white', height: '80%'}}/>
                    <Button textStyle={[styles.menuButtonText, {...this.state.selectedFilter === "S" && {textDecorationLine:'underline'}} ]} buttonStyle={{width: '50%'}} text={"Stores"}
                            action={() => this.onStoreButtonClicked()} />
                </View>
                <QRDialog isVisible={this.state.qrDialogVisibility} onClose={() => this.setState({qrDialogVisibility: false})} />
            </View>
        );
    }

    onCollPointButtonClicked() {
        this.props.fetchCollectionPoints({
            data: {
                lat: this.state.latitude,
                lng: this.state.longitude,
                radius: 500
            }
        });
        this.setState({selectedFilter: "CP"});
    }

    onStoreButtonClicked() {
        this.props.fetchStores({
            data: {
                lat: this.state.latitude,
                lng: this.state.longitude,
                radius: 500
            }
        });
        this.setState({selectedFilter: "S"});
    }

    myAccountOnClick() {
        this.props.navigation.openDrawer();
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
    },
    header: {
        height: 60,
        alignItems: 'center',
        flexDirection: "row",
        justifyContent: "flex-end",
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.41,
        shadowRadius: 9.11,
        elevation: 14,
    },
    headerItem: {
      marginRight: 15
    },
    headerTitle: {
        flex: 1,
        textAlign: "left",
        marginLeft: 15,
        marginTop: 5,
        fontSize: 18,
        // fontFamily: 'Roboto',
    },
    mapContainer: {
        flex: 1,
    },
    map: {
        flex: 1,
        borderRadius: 10,
        borderWidth: 5
    },
    expandMenu: {
        height: 60,
        flexDirection: 'row',
        justifyContent: 'space-evenly',
        alignItems: 'center',
        fontSize: 18,
        backgroundColor: THEME_GREEN,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 7,
        },
        shadowOpacity: 0.41,
        shadowRadius: 9.11,
        elevation: 14,
    },
    menuButtonText: {
        color: 'white',
    }
});

const mapStateToProps = (state) => ({
    collectionPoints: getCollectionPoints(state),
    stores: getStores(state),
    user: getUser(state)
});

const mapDispatchToProps = {
    fetchCollectionPoints: makeAction(FETCH_COLLECTION_POINTS),
    fetchStores: makeAction(FETCH_STORES),
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(HomeScreen);