import React, {Component} from 'react';
import Button from '../../components/Button';
import { THEME_GREEN } from '../../res/colors';
import { StyleSheet, View, Text, Platform, TouchableOpacity, Linking, PermissionsAndroid } from 'react-native';
import { CameraKitCameraScreen, } from 'react-native-camera-kit';
import Permissions from "react-native-permissions";
import {Icon} from "react-native-elements";

class CollectionPointOwnerHomeScreen extends Component {
  static navigationOptions = {
    drawerLabel: 'Scan QR',
    drawerIcon: () => (
        <Icon
            type="font-awesome"
            name='qrcode'
            size={25}
            color="#4CAF50" />
    ),
  };

  constructor(props) {
    super(props);

    this.state = {
      QR_Code_Value: '',
      Start_Scanner: false,

    };
  }
  openLink_in_browser = () => {

    Linking.openURL(this.state.QR_Code_Value);
  };

  onQR_Code_Scan_Done = (QR_Code) => {

    this.setState({ QR_Code_Value: QR_Code });
    this.setState({ Start_Scanner: false });
  };

  _requestPermission = () => {
    Permissions.request('camera').then(response => {
      this.setState({hasPermission: response === "authorized"});
    })
  };

  open_QR_Code_Scanner=()=> {

    Permissions.check('camera').then(response => {
      if (response === "authorized") {
        this.setState({QR_Code_Value: '', Start_Scanner: true});
      } else {
        this._requestPermission();
      } });
  };


  render() {
    if (!this.state.Start_Scanner) {

      return (
            <View style={styles.MainContainer}>

              <Text style={{ fontSize: 22, textAlign: 'center' }}>Scan code</Text>

              <Text style={styles.QR_text}>
                {this.state.QR_Code_Value ? this.state.QR_Code_Value : ''}
              </Text>

              {this.state.QR_Code_Value.includes("http") ?
                  <TouchableOpacity
                      onPress={this.openLink_in_browser}
                      style={styles.button}>
                    <Text style={{ color: '#FFF', fontSize: 14 }}>Open Link in Browser</Text>
                  </TouchableOpacity> : null
              }

              <TouchableOpacity
                  onPress={this.open_QR_Code_Scanner}
                  style={styles.button}>
                <Text style={{ color: '#FFF', fontSize: 14 }}>
                  Open QR Scanner
                </Text>
              </TouchableOpacity>

            </View>
      );
    }
    return (
        <View style={{ flex: 1 }}>

          <CameraKitCameraScreen
              showFrame={true}
              scanBarcode={true}
              laserColor={'#FF3D00'}
              frameColor={'#00C853'}
              colorForScannerFrame={'black'}
              onReadCode={event =>
                  this.onQR_Code_Scan_Done(event.nativeEvent.codeStringValue)
              }
          />
        </View>
    )}
}

const styles = StyleSheet.create({
  MainContainer: {
    flex: 1,
    paddingTop: (Platform.OS) === 'ios' ? 20 : 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  QR_text: {
    color: '#000',
    fontSize: 19,
    padding: 8,
    marginTop: 12
  },
  button: {
    backgroundColor: '#2979FF',
    alignItems: 'center',
    padding: 12,
    width: 300,
    marginTop: 14
  },
});

export default CollectionPointOwnerHomeScreen;