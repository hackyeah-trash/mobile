import {StyleSheet, Image, View, Text, Dimensions} from "react-native";
import React from "react";
import {Icon} from 'react-native-elements';
import {THEME_GREEN} from "../../res/colors";

export default class MyEcopointsScreen extends React.Component {
    static navigationOptions = {
        drawerLabel: 'MyEcopoints',
        drawerIcon: () => (
            <Icon
                type="entypo"
                name='tree'
                size={25}
                color="#4CAF50" />
        ),
    };

    constructor(props) {
        super(props);
        this.state = {

        };
    }

    render() {
        return (
            <View style={styles.body}>
                <View style={styles.seedlingImage}>
                    <Image source={require('../../assets/seedling.png')} style={{width: 210, height: 210}} resizeMode={"contain"} />
                </View>
                <View style={styles.infoTextContainer}>
                    <Text style={styles.infoTextDesc}>Twoje punkty: </Text>
                    <Text style={styles.infoText}>1500 ecopunktów</Text>

                    <Text style={styles.infoTextDesc}>Aby Twoja roślinka urosła brakuje ci:</Text>
                    <Text style={styles.infoText}>2800 ecopunktów</Text>
                </View>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    body: {
        flex: 1,
        backgroundColor: "#7aae47",
        alignItems: 'center',
    },
    seedlingImage: {
        borderRadius: Math.round(Dimensions.get('window').width + Dimensions.get('window').height) / 2,
        width: Dimensions.get('window').width * 0.8,
        height: Dimensions.get('window').width * 0.8,
        backgroundColor: '#ffffff',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        marginTop: 10,
        borderWidth: 5,
        borderColor: '#ffffff'
    },
    infoTextContainer: {
        marginTop: 30,
        textAlign: 'center',
        justifyContent: 'center',
        alignItems: 'center'
    },
    infoText: {
        fontSize: 28,
        // fontFamily: 'Roboto',
        color: 'white'
    },
    infoTextDesc: {
        fontSize: 18,
        // fontFamily: 'Roboto',
        color: 'white',
        marginTop: 30
    }
});