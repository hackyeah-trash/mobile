import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text
} from 'react-native';

import Swiper from 'react-native-swiper';
import Icon from 'react-native-vector-icons/MaterialCommunityIcons';
import Button from '../../components/Button';
import {THEME_GREEN} from '../../res/colors';
import LinearGradient from 'react-native-linear-gradient';

class WelcomeScreen extends Component {
  static navigationOptions = {
    drawerLabel: () => null,
    gesturesEnabled: false,
    swipeEnabled: false,
    drawerLockMode: 'locked-closed'
  };

  render() {
    return (

      <LinearGradient style={styles.container} start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#aaffa9', '#11ffbd']}>
        <Swiper showsButtons={true}>
          <View style={styles.slide}>
            <Text style={styles.heading}>Do something for environment, and at the same time for yourself!</Text>
            <Icon name={'earth'} size={150} color={'#fff'}/>
          </View>
          <View style={styles.slide}>
            <Text style={styles.heading}>Give items back in selected places</Text>
            <Icon name={'recycle'} size={150} color={'#fff'}/>
          </View>
          <View style={styles.slide}>
            <Text style={styles.heading}>Get ecopoints</Text>
            <Icon name={'numeric-6-circle'} size={150} color={'#fff'}/>
          </View>
          <View style={styles.slide}>
            <Text style={styles.heading}>Exhange ecopoints for promotions</Text>
            <Icon name={'shopping'} size={150} color={'#fff'}/>
          </View>
        </Swiper>
        <View style={styles.buttonsContainer}>
          <Button 
            text="Sign in"
            buttonStyle={[
              styles.btn, 
              {backgroundColor: THEME_GREEN}
            ]}
            textStyle={[
              styles.btnText,
              {color: '#fff'}
            ]}
            action={() => this.props.navigation.navigate('Login')}
          />
          <Button 
            text="Sign up"
            buttonStyle={[
              styles.btn, 
              {
                borderWidth: 1, 
                borderColor: THEME_GREEN,
                backgroundColor: '#fff',
              }
            ]}
            textStyle={[
              styles.btnText,
              {color: THEME_GREEN}
            ]}
            action={() => this.props.navigation.navigate('Register')}
          />
        </View>
      </LinearGradient>
    )
  }
}

const styles = StyleSheet.create({
  container:{
    flex: 1,
  },
  slide: {
    flex: 1,
    padding: '5%',
    justifyContent: 'center',
    alignItems: 'center',
    // backgroundColor: '#fff'
  },
  heading: {
    fontSize: 30,
    textAlign: 'center',
    marginBottom: 30,
    color: '#fff',
  },
  buttonsContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  btn: {
    width: '50%',
    padding: 10,
    height: 50,
    justifyContent: 'center'
  },
  btnText: {
    fontSize: 20,
  }
});

export default WelcomeScreen;