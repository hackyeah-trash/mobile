import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  Text,
  TextInput,
  Alert,
  ScrollView,
  KeyboardAvoidingView
} from 'react-native';

import {THEME_GREEN} from '../../res/colors';
import Button from '../../components/Button'; 
import {connect} from 'react-redux';
import { makeAction } from '../../redux/actions/makeAction';
import { REGISTER_USER } from '../../redux/actions/actionTypes';

class RegistrationFirstPart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      firstName: '',
      lastName: '',
      username: '',
      email: '',
      password: '',
    }
  }
  changeText(text, field) {
    this.setState({
      [field]: text
    });
  }
  render() {
    const {registerUser} = this.props;
    return(
      <KeyboardAvoidingView style={styles.container} behavior="padding" enabled>
        <ScrollView 
        style={styles.container}
        contentContainerStyle={styles.srollContainer}
        >
          <Text style={styles.header}>Sign up</Text>
          <View style={styles.inputsContainer}>
            <TextInput 
              value={this.state.firstName}
              style={styles.input} 
              placeholder="First name" 
              autoCapitalize="none"
              onChangeText={(newText) => this.changeText(newText, 'firstName')}
            />
            <TextInput 
              value={this.state.lastName}
              style={styles.input} 
              placeholder="Last name" 
              autoCapitalize="none"
              onChangeText={(newText) => this.changeText(newText, 'lastName')}
            />
            <TextInput 
              value={this.state.username}
              style={styles.input} 
              placeholder="Username" 
              autoCapitalize="none"
              onChangeText={(newText) => this.changeText(newText, 'username')}
            />
            <TextInput 
              value={this.state.email}
              style={styles.input} 
              placeholder="Email" 
              autoCapitalize="none"
              keyboardType="email-address"
              onChangeText={(newText) => this.changeText(newText, 'email')}
            />
            <TextInput
              value={this.state.password}
              style={styles.input} 
              placeholder="Password" 
              autoCapitalize="none"
              secureTextEntry={true}
              onChangeText={(newText) => this.changeText(newText, 'password')}
            />
          </View>
          <Button 
            text="Next"
            buttonStyle={styles.registerBtn}
            textStyle={styles.registerBtnText}
            action={() => {
              registerUser({
                data: this.state,
                successCallback: () => {
                  this.props.navigation.navigate('Login');
                  Alert.alert('Sukces!');
                }
              })
            }}
          />
        </ScrollView>
      </KeyboardAvoidingView>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '5%',
  },
  srollContainer: {
    flex: 1,
    justifyContent: 'center'
  },
  header: {
    alignSelf: 'center',
    fontSize: 25,
    color: THEME_GREEN,
    textAlign: 'center',
    textTransform: 'uppercase',
    marginBottom: 30
  },
  inputsContainer: {
    // flex: 5,
  },
  input: {
    fontSize: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: THEME_GREEN,
    margin: 10,
    padding: 10
  },
  registerBtn: {
    alignSelf: 'center',
    backgroundColor: THEME_GREEN,
    borderRadius: 10,
    padding: 10,
    marginTop: 30,
    width: '60%'
  },
  registerBtnText: {
    fontSize: 20,
    color: '#fff'
  }
});

const mapDispatchToProps = {
  registerUser: makeAction(REGISTER_USER),
};

export default connect(
  null,
  mapDispatchToProps
)(RegistrationFirstPart);
