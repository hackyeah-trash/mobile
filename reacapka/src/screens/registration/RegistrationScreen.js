import React, {Component} from 'react';
import {
  View,
  StyleSheet,
} from 'react-native';

import {THEME_GREEN} from '../../res/colors'; 
import RegistrationFirstPart from './RegistrationFirstPart';
import {Icon} from "react-native-elements";

class RegistrationScreen extends Component {
  static navigationOptions = {
    drawerLabel: () => null,
    gesturesEnabled: false,
    swipeEnabled: false,
    drawerLockMode: 'locked-closed'
  };

  render() {
    return (
      <View style={styles.container}>
        <RegistrationFirstPart/>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '5%',
  },
});

export default RegistrationScreen;