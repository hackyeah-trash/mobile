import React, {Component} from 'react';
import {StyleSheet, Text, TextInput, View} from 'react-native';

import {THEME_GREEN} from '../../res/colors';
import Button from '../../components/Button';
import {Icon} from "react-native-elements";
import {makeAction} from "../../redux/actions/makeAction";
import {AUTH_USER} from "../../redux/actions/actionTypes";
import {connect} from "react-redux";

class RegistrationFirstPart extends Component {
  static navigationOptions = {
    drawerLabel: () => null,
    gesturesEnabled: false,
    swipeEnabled: false,
    drawerLockMode: 'locked-closed'
  };

  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
    }
  }
  changeText(text, field) {
    this.setState({
      [field]: text
    });
  }
  render() {
    const {authUser} = this.props;
    return(
        <View style={styles.container}>
          <Text style={styles.header}>Sign in</Text>
          <View style={styles.inputsContainer}>
            <TextInput 
              value={this.state.username}
              style={styles.input} 
              placeholder="Username" 
              onChangeText={(newText) => this.changeText(newText, 'username')}
              autoCapitalize="none"
            />
            <TextInput
              value={this.state.password}
              style={styles.input} 
              placeholder="Password" 
              onChangeText={(newText) => this.changeText(newText, 'password')}
              autoCapitalize="none"
            />
          </View>
          <Button 
            text="Sign in"
            buttonStyle={styles.registerBtn}
            textStyle={styles.registerBtnText}
            action={() => authUser({
              data: this.state,
              successCallback: () => this.props.navigation.navigate('Home')
            })}
          />
        </View>
    );
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '5%',
    justifyContent: 'center'
  },
  header: {
    position: 'absolute',
    alignSelf: 'center',
    top: 20,
    fontSize: 25,
    color: THEME_GREEN,
    textAlign: 'center',
    textTransform: 'uppercase',
    marginBottom: 30
  },
  inputsContainer: {
    // flex: 5,
  },
  input: {
    fontSize: 20,
    borderWidth: 1,
    borderRadius: 10,
    borderColor: THEME_GREEN,
    margin: 10,
    padding: 10
  },
  registerBtn: {
    alignSelf: 'center',
    backgroundColor: THEME_GREEN,
    marginTop: 30,
    borderRadius: 10,
    padding: 10,
    width: '60%'
  },
  registerBtnText: {
    fontSize: 20,
    color: '#fff'
  }
});

const mapDispatchToProps = {
  authUser: makeAction(AUTH_USER),
};

export default connect(
  null,
  mapDispatchToProps
)(RegistrationFirstPart);
