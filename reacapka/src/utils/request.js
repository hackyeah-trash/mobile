import request from 'axios';
import { call } from 'redux-saga/effects';
import { getAccessToken } from '../redux/selectors/authSelector';
import {store} from '../../App';

export const GET = 'GET';
export const POST = 'POST';
export const PUT = 'PUT';
export const DELETE = 'DELETE';

const service = (requestType, url, data = {}, config = {}) => {
  const token = getAccessToken(store.getState());
  request.defaults.headers.common.Accept = 'application/json';
  request.defaults.headers.common.Authorization = token;

  switch (requestType) {
      case GET: {
          return call(request.get, url, config);
      }
      case POST: {
          return call(request.post, url, data, config);
      }
      case PUT: {
          return call(request.put, url, data, config);
      }
      case DELETE: {
          return call(request.delete, url, config);
      }
      default: {
          throw new TypeError('No valid request type provided');
      }
  }
};

export default service;
