import {createSelector} from 'reselect';

const branchSelector = store => store.stores;

export const getStores = createSelector(
  branchSelector,
  branch => branch.stores,
);
