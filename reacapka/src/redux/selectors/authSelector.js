import {createSelector} from 'reselect';

const branchSelector = store => store.auth;

export const getAccessToken = createSelector(
  branchSelector,
  branch => branch.token,
);

export const getUser = createSelector(
  branchSelector,
  branch => branch.user
);

//Todo: finish this method
export const getIsCollectionPointOwner = createSelector(
  getUser,
  (user) => {
    console.log(user);
    return user;
  }
)