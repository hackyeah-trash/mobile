import {createSelector} from 'reselect';

const branchSelector = store => store.materials;

export const getMaterials = createSelector(
  branchSelector,
  branch => branch.materials,
);
