import {createSelector} from 'reselect';

const branchSelector = store => store.collectionPoints;

export const getCollectionPoints = createSelector(
  branchSelector,
  branch => branch.collectionPoints,
);
