import {createSelector} from 'reselect';

const branchSelector = store => store.ecopoints;

export const getEcopoints = createSelector(
  branchSelector,
  branch => branch.ecopoints,
);
