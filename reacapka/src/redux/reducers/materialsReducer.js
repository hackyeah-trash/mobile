import {FETCH_MATERIALS_SUCCESS} from '../actions/actionTypes';

const DEFAULT_STATE = {
  materials: []
};

const reducer = (state=DEFAULT_STATE, action) => {
  switch(action.type) {
    case FETCH_MATERIALS_SUCCESS:
      return {
        materials: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
