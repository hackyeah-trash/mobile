import {combineReducers} from 'redux';

import auth from './authReducer';
import collectionPoints from './collectionPointsReducer';
import stores from './storesReducer'
import materials from './materialsReducer'

const root = combineReducers({
  auth,
  collectionPoints,
  stores,
  materials
});

export default root;
