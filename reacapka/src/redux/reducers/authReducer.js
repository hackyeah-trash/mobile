import {AUTH_USER_GET_SUCCESS, AUTH_USER_SUCCESS, LOGOUT_USER} from '../actions/actionTypes';
const DEFAULT_STATE = {
  token: null,
  user: null
};

const reducer = (state=DEFAULT_STATE, action) => {
  switch(action.type) {
    case AUTH_USER_SUCCESS:
      return {
        ...state,
        token: action.payload
      };
    case AUTH_USER_GET_SUCCESS:
      return {
        ...state,
        user: action.payload
      };
    case LOGOUT_USER: 
      return DEFAULT_STATE;
    default: 
      return state;
  }
}

export default reducer;
