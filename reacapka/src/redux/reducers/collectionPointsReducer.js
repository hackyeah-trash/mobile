import { FETCH_COLLECTION_POINTS_SUCCESS} from '../actions/actionTypes';

const DEFAULT_STATE = {
  collectionPoints: []
};

const reducer = (state=DEFAULT_STATE, action) => {
  switch(action.type) {
    case FETCH_COLLECTION_POINTS_SUCCESS:
      return {
        collectionPoints: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
