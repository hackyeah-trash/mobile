import {FETCH_STORES_SUCCESS, FETCH_ECOPOINTS_SUCCESS} from '../actions/actionTypes';

const DEFAULT_STATE = {
  ecopoints: 0
};

const reducer = (state=DEFAULT_STATE, action) => {
  switch(action.type) {
    case FETCH_ECOPOINTS_SUCCESS:
      return {
        ecopoints: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
