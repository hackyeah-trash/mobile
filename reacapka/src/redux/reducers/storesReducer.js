import {FETCH_STORES_SUCCESS} from '../actions/actionTypes';

const DEFAULT_STATE = {
  stores: []
};

const reducer = (state=DEFAULT_STATE, action) => {
  switch(action.type) {
    case FETCH_STORES_SUCCESS:
      return {
        stores: action.payload
      };
    default:
      return state;
  }
};

export default reducer;
