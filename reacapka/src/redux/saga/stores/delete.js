import request, {DELETE} from '../../../utils/request';
import {storeUrl} from "../../../res/urls";
import {DELETE_STORE_FAILURE, DELETE_STORE_SUCCESS} from "../../actions/actionTypes";
import {put} from "@redux-saga/core/effects";

export default function *deleteStore(action) {
    try {
        yield request(DELETE, storeUrl(action.payload.id));
        yield put({
            type: DELETE_STORE_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: DELETE_STORE_FAILURE
        });
    }
}
