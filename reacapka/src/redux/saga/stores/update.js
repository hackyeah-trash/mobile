import request, {PUT} from '../../../utils/request';
import {storeUrl} from "../../../res/urls";
import {UPDATE_STORE_FAILURE, UPDATE_STORE_SUCCESS} from "../../actions/actionTypes";
import {put} from "@redux-saga/core/effects";

export default function *updateStore(action) {
    try {
        yield request(PUT, storeUrl(action.payload.id), action.payload.store);
        yield put({
            type: UPDATE_STORE_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: UPDATE_STORE_FAILURE
        });
    }
}
