import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, registerUrl} from "../../../res/urls";
import {FETCH_COLLECTION_POINTS_FAILURE, FETCH_COLLECTION_POINTS_SUCCESS, REGISTER_USER_FAILURE} from "../../actions/actionTypes";
import {put} from "@redux-saga/core/effects";

export default function *fetchCollectionPoints(action) {
    try {
        const response = yield request(GET, collectionPointsUrl, action.payload);
        yield put({
            type: FETCH_COLLECTION_POINTS_SUCCESS,
            payload: response.body
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: FETCH_COLLECTION_POINTS_FAILURE
        });
    }
}
