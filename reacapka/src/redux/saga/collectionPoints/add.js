import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, registerUrl} from "../../../res/urls";
import {
    ADD_COLLECTION_POINT_FAILURE,
    ADD_COLLECTION_POINT_SUCCESS,
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS,
    REGISTER_USER_FAILURE
} from "../../actions/actionTypes";

export default function *addCollectionPoint(action) {
    try {
        yield request(POST, collectionPointsUrl, action.payload.collectionPoint);
        yield put({
            type: ADD_COLLECTION_POINT_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: ADD_COLLECTION_POINT_FAILURE
        });
    }
}
