import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, collectionPointUrl, registerUrl} from "../../../res/urls";
import {
    ADD_COLLECTION_POINT_FAILURE,
    ADD_COLLECTION_POINT_SUCCESS,
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS,
    REGISTER_USER_FAILURE, UPDATE_COLLECTION_POINT_FAILURE, UPDATE_COLLECTION_POINT_SUCCESS
} from "../../actions/actionTypes";
import {put} from "@redux-saga/core/effects";

export default function *updateCollectionPoint(action) {
    try {
        yield request(PUT, collectionPointUrl(action.payload.id), action.payload.collectionPoint);
        yield put({
            type: UPDATE_COLLECTION_POINT_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: UPDATE_COLLECTION_POINT_FAILURE
        });
    }
}
