import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, ecopointsUrl, registerUrl} from "../../../res/urls";
import {
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS, FETCH_ECOPOINTS_FAILURE,
    FETCH_ECOPOINTS_SUCCESS,
    REGISTER_USER_FAILURE
} from "../../actions/actionTypes";
import {getUser} from "../../selectors/authSelector";
import {put} from "@redux-saga/core/effects";

export default function *fetchUserEcopoints() {
    try {
        const username = getUser(store.getState());
        const response = yield request(GET, ecopointsUrl(username));
        yield put({
            type: FETCH_ECOPOINTS_SUCCESS,
            payload: response.body
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: FETCH_ECOPOINTS_FAILURE
        });
    }
}
