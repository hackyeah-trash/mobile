import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, ecopointsUrl, registerUrl} from "../../../res/urls";
import {
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS,
    FETCH_ECOPOINTS_FAILURE,
    FETCH_ECOPOINTS_SUCCESS,
    REGISTER_USER_FAILURE,
    SUBMIT_ECOPOINTS_FAILURE,
    SUBMIT_ECOPOINTS_SUCCESS,
    WITHDRAW_ECOPOINTS_FAILURE,
    WITHDRAW_ECOPOINTS_SUCCESS
} from "../../actions/actionTypes";
import {getUser} from "../../selectors/authSelector";
import {put} from "@redux-saga/core/effects";

export default function *withdrawUserEcopoints() {
    try {
        const username = getUser(store.getState());
        yield request(DELETE, ecopointsUrl(username), action.amount);
        yield put({
            type: WITHDRAW_ECOPOINTS_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: WITHDRAW_ECOPOINTS_FAILURE
        });
    }
}
