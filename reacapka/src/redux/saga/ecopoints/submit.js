import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, ecopointsUrl, registerUrl} from "../../../res/urls";
import {
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS, FETCH_ECOPOINTS_FAILURE,
    FETCH_ECOPOINTS_SUCCESS,
    REGISTER_USER_FAILURE, SUBMIT_ECOPOINTS_FAILURE, SUBMIT_ECOPOINTS_SUCCESS
} from "../../actions/actionTypes";
import {getUser} from "../../selectors/authSelector";
import {put} from "@redux-saga/core/effects";

export default function *submitUserEcopoints() {
    try {
        const username = getUser(store.getState());
        yield request(POST, ecopointsUrl(username), action.delivery);
        yield put({
            type: SUBMIT_ECOPOINTS_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: SUBMIT_ECOPOINTS_FAILURE
        });
    }
}
