import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {registerUrl} from "../../../res/urls";
import {REGISTER_USER_FAILURE} from "../../actions/actionTypes";
import {put} from 'redux-saga/effects';

export default function *register(action) {
    const data = {
        username: action.payload.data.username,
        password: action.payload.data.password,
        firstName: action.payload.data.firstName,
        lastName: action.payload.data.lastName,
        email: action.payload.data.email
    }
    console.log(data);
    try {
        yield request(POST, registerUrl, data);
        yield put({
            type: REGISTER_USER_SUCCESS
        });

        action.payload.successCallback();
    }
    catch (error) {
        console.log(error);
        yield put({
            type: REGISTER_USER_FAILURE
        });
    }
}
