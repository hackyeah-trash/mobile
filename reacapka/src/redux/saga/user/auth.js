import request, {GET, POST} from "../../../utils/request";
import {authUrl, userUrl} from "../../../res/urls";
import * as actionTypes from "../../actions/actionTypes";
import {put} from 'redux-saga/effects';

export default function *authUser(action) {
    const data = {
        username: action.payload.data.username,
        password: action.payload.data.password,
    };
    try {
        const authResponse = yield request(POST, authUrl, data);
        const token = authResponse.headers.authorization;
        yield put({
            type: actionTypes.AUTH_USER_SUCCESS,
            payload: token,
        });

        const userResponse = yield request(GET, userUrl(action.payload.data.username));
        yield put({
            type: actionTypes.AUTH_USER_GET_SUCCESS,
            payload: userResponse.body
        });

        action.payload.successCallback();

    } catch(error) {
        console.log(error);
        yield put({
            type: actionTypes.AUTH_USER_FAILURE
        })
    }
}
