import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, materialsUrl, materialUrl, registerUrl, storesUrl} from "../../../res/urls";
import {
    ADD_MATERIAL_FAILURE,
    ADD_MATERIAL_SUCCESS, DELETE_MATERIAL_FAILURE, DELETE_MATERIAL_SUCCESS,
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS, FETCH_MATERIALS_FAILURE, FETCH_MATERIALS_SUCCESS, FETCH_STORES_FAILURE,
    FETCH_STORES_SUCCESS,
    REGISTER_USER_FAILURE
} from "../../actions/actionTypes";
import {put} from "@redux-saga/core/effects";

export default function *deleteMaterial(action) {
    try {
        yield request(DELETE, materialUrl(action.payload.id));
        yield put({
            type: DELETE_MATERIAL_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: DELETE_MATERIAL_FAILURE
        });
    }
}
