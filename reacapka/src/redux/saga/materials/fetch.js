import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, materialsUrl, registerUrl, storesUrl} from "../../../res/urls";
import {
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS, FETCH_MATERIALS_FAILURE, FETCH_MATERIALS_SUCCESS, FETCH_STORES_FAILURE,
    FETCH_STORES_SUCCESS,
    REGISTER_USER_FAILURE
} from "../../actions/actionTypes";
import {put} from "@redux-saga/core/effects";

export default function *fetchMaterials() {
    try {
        const response = yield request(GET, materialsUrl);
        yield put({
            type: FETCH_MATERIALS_SUCCESS,
            payload: response.body
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: FETCH_MATERIALS_FAILURE
        });
    }
}
