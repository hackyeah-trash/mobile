import request, {DELETE, GET, POST, PUT} from '../../../utils/request';
import {collectionPointsUrl, materialsUrl, materialUrl, registerUrl, storesUrl} from "../../../res/urls";
import {
    ADD_MATERIAL_FAILURE,
    ADD_MATERIAL_SUCCESS,
    FETCH_COLLECTION_POINTS_FAILURE,
    FETCH_COLLECTION_POINTS_SUCCESS, FETCH_MATERIALS_FAILURE, FETCH_MATERIALS_SUCCESS, FETCH_STORES_FAILURE,
    FETCH_STORES_SUCCESS,
    REGISTER_USER_FAILURE, UPDATE_MATERIAL_FAILURE, UPDATE_MATERIAL_SUCCESS
} from "../../actions/actionTypes";
import {put} from "@redux-saga/core/effects";

export default function *updateMaterial(action) {
    try {
        yield request(PUT, materialUrl(action.payload.id), action.payload.material);
        yield put({
            type: UPDATE_MATERIAL_SUCCESS
        });
    }
    catch (error) {
        console.log(error);
        yield put({
            type: UPDATE_MATERIAL_FAILURE
        });
    }
}
