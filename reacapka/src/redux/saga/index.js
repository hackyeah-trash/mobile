import {all, takeLatest,} from 'redux-saga/effects';
import * as actionTypes from '../actions/actionTypes';
import authUser from "./user/auth";
import registerUser from './user/register';
import fetchCollectionPoints from './collectionPoints/fetch';
import fetchMaterials from './materials/fetch';
import fetchStores from './stores/fetch';

export default function *rootSaga() {
  yield all([
    yield takeLatest(actionTypes.AUTH_USER, authUser),
    yield takeLatest(actionTypes.REGISTER_USER, registerUser),
    yield takeLatest(actionTypes.FETCH_COLLECTION_POINTS, fetchCollectionPoints),
    yield takeLatest(actionTypes.FETCH_MATERIALS, fetchMaterials),
    yield takeLatest(actionTypes.FETCH_STORES, fetchStores),
  ]);
}
