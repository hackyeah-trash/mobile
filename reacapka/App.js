import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StatusBar,
} from 'react-native';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga';
import { composeWithDevTools } from 'redux-devtools-extension';

import rootReducer from './src/redux/reducers';
import rootSaga from './src/redux/saga/index';
import DrawerNavigator from "./src/components/DrawerNavigator";

const sagaMiddleware = createSagaMiddleware();

export const store = createStore(
  rootReducer,
  composeWithDevTools (
    applyMiddleware(sagaMiddleware)
  )
);

sagaMiddleware.run(rootSaga);

const App = () => {
  return (
    <Provider store={store}>
      <Fragment>
        <StatusBar barStyle="light-content" />
        <SafeAreaView style={{flex: 1}}>
          <DrawerNavigator />
        </SafeAreaView>
      </Fragment>
    </Provider>
  );
};

export default App;
